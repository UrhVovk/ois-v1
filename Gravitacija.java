import java.util.*;
public class Gravitacija
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        double constC = 6.674 * Math.pow(10,-11);
        double constM = 5.972 * Math.pow(10,24);
        double constr = 6.371 * Math.pow(10,6);
        izpis(sc.nextDouble());
    }
    
    private static double gravitacijskiPospesek(double nadmorskaVisina)
    {
        return ((6.674 * Math.pow(10,-11)) * (5.972 * Math.pow(10,24)))
            /Math.pow((nadmorskaVisina + 6.371 * Math.pow(10,6)),2);
    }
    
    private static void izpis(double nadmorskaVisina)
    {
        System.out.println("Gravitacijski pospesek: " + gravitacijskiPospesek(nadmorskaVisina));
    }
}